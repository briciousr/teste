//
//  MovieObjectMapper.swift
//  testeBradesco
//
//  Created by Fabricio Rodrigues on 11/04/2018.
//  Copyright © 2018 Fabricio Rodrigues. All rights reserved.
//

import UIKit
import ObjectMapper


struct Movies: Mappable {
    var movies: [MovieObjectMapper]
    
    init?(map: Map) {
        do {
            movies = try map.value("results")
        } catch {
            return nil
        }
    }
    
    mutating func mapping(map: Map) {
        movies <- map["results"]
    }
}

struct MovieObjectMapper: Mappable {
    
    var title: String?
    var vote_average: Float?
    var poster_path: String!
    var array: [Any]?
    var release_date: String?
    var overview: String?
    var imageName: String?
    
    init?(map: Map) {

    }
    
    // Mappable
    mutating func mapping(map: Map) {
        
        title    <- map["title"]
        vote_average         <- map["vote_average"]
        poster_path      <- map["poster_path"]
        array       <- map["results"]
        release_date  <- map["release_date"]
        overview  <- map["overview"]
        imageName  <- map["poster_path"]
    }
    
    
}


//
//  MoviesCollectionViewCell.swift
//  testeBradesco
//
//  Created by Fabricio Rodrigues on 11/04/2018.
//  Copyright © 2018 Fabricio Rodrigues. All rights reserved.
//

import UIKit

class MoviesCollectionViewCell: UICollectionViewCell {
 
    @IBOutlet weak var imageViewMovie: UIImageView!
    @IBOutlet weak var imageViewRating: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    
}

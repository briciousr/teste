//
//  MoviesRequest.swift
//  testeBradesco
//
//  Created by Fabricio Rodrigues on 11/04/2018.
//  Copyright © 2018 Fabricio Rodrigues. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import PKHUD


class MoviesRequest: NSObject {
    
    var projects:[MovieObjectMapper] = []
    
    class func getListMovies(page: Int, completion: @escaping ([MovieObjectMapper]?, Error?) -> Void) {
        
        let urlString:String = String.init(format: "https://api.themoviedb.org/3/movie/now_playing?api_key=c8676c30c72e30216028908b86a31b13&language=en-US&page=%i",page)
        
        PKHUD.sharedHUD.contentView = PKHUDProgressView()
        PKHUD.sharedHUD.show()
        
        Alamofire.request(urlString, parameters: nil)
            .validate(contentType: ["application/json"]).responseObject { (response: DataResponse<Movies>) in
                switch(response.result){
                    
                case Result.success:
                    if let response = response.result.value {
                        completion(response.movies, nil)
                    }
                    break
                    
                default:
                    completion(nil, response.error)
                    break
                }
                
                PKHUD.sharedHUD.hide()       
        }
    }
    
}

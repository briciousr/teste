//
//  MoviesViewController.swift
//  testeBradesco
//
//  Created by Fabricio Rodrigues on 11/04/2018.
//  Copyright © 2018 Fabricio Rodrigues. All rights reserved.
//

import UIKit

class MoviesViewController: BaseViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet weak var collectionView:UICollectionView!
    
    var receiveCategory:String!
    var pageMovie:Int!
    private var listMovies = Array<MovieObjectMapper>()
    var objSelected:MovieObjectMapper!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pageMovie = 1
        
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.checkDataMovies()
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listMovies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:MoviesCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "movieCell", for: indexPath) as! MoviesCollectionViewCell
        
        let obj:MovieObjectMapper = self.listMovies[indexPath.row]
        cell.labelTitle.text = obj.title
        
        let urlImage: String = String.init(format: "http://image.tmdb.org/t/p/w185/%@",obj.imageName!)
        cell.imageViewMovie.sd_setImage(with: URL(string: urlImage), placeholderImage: UIImage(named: "icon_movie"))
        
        return cell
        
    }
    
    // Permite realizar a paginação dos filmes
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {

        if (indexPath.row == listMovies.count - 1) {
            pageMovie = pageMovie + 1
            self.requestMovie(page: pageMovie)
        }

    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        objSelected = self.listMovies[indexPath.row]
        
        if objSelected != nil {
            self.performSegue(withIdentifier: "segueDetails", sender: self)
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueDetails" {
            var vc:DetailsViewController = DetailsViewController()
            vc = segue.destination as! DetailsViewController
            vc.objReceived = objSelected
        } else {
            var vc:SearchViewController = SearchViewController()
            vc = segue.destination as! SearchViewController
            vc.listMovies = self.listMovies
        }
    }
    
    func requestMovie(page: Int) -> Void {
        
        MoviesRequest.getListMovies(page: page) { (movies, error) in
            
            if let newListMovies = movies {
                
                self.listMovies.append(contentsOf: newListMovies)
                
                self.listMovies = self.listMovies.filter{
                    ($0.vote_average != nil && $0.vote_average! > 5)
                }

                let userDefaults:UserDefaults = UserDefaults.standard
                let jsonString = self.listMovies.toJSONString()
                userDefaults.set(jsonString, forKey: "moviesSaved")
                
                self.collectionView.reloadData()
                
            } else if(movies == nil) {
                self.alertDados()
            }
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func callSearch(){
        self.performSegue(withIdentifier: "segueSearch", sender: self)
    }
    
}

extension MoviesViewController {
    
    func checkDataMovies(){
        let userDefaults:UserDefaults = UserDefaults.standard
        
        if (userDefaults.string(forKey: "moviesSaved") != nil) {
            
            let jsonString = userDefaults.string(forKey: "moviesSaved")
            
            do {
                self.listMovies = Array<MovieObjectMapper>.init(JSONString: jsonString!)!
                
                self.collectionView.reloadData()
            }
            
        } else {
            requestMovie(page: pageMovie)
        }
    }
}


//
//  DetailsViewController.swift
//  testeBradesco
//
//  Created by Fabricio Rodrigues on 11/04/2018.
//  Copyright © 2018 Fabricio Rodrigues. All rights reserved.
//
import UIKit
import SDWebImage

class DetailsViewController: BaseViewController {
    
    @IBOutlet weak var imageMovie:UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var textViewOverview:UITextView!
    
    var objReceived:MovieObjectMapper!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let urlImage: String = String.init(format: "http://image.tmdb.org/t/p/w185/%@",objReceived.imageName!)
        imageMovie.sd_setImage(with: URL(string: urlImage), placeholderImage: UIImage(named: "icon_movie"))
        
        labelTitle.text = objReceived.title
        textViewOverview.text = objReceived.overview
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}

//
//  SearchViewController.swift
//  testeBradesco
//
//  Created by Fabricio on 12/04/2018.
//  Copyright © 2018 Fabricio Rodrigues. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchResultsUpdating  {
    
    @IBOutlet weak var tableView:UITableView!
    var listMovies:Array<MovieObjectMapper>!
    var moviesResults:Array<MovieObjectMapper>!
    var objSelected:MovieObjectMapper!
    
    let searchController = UISearchController(searchResultsController: nil)

    override func viewDidLoad() {
        super.viewDidLoad()

        moviesResults = listMovies
        tableView.delegate = self
        tableView.dataSource = self
        
        searchController.searchResultsUpdater = self
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.dimsBackgroundDuringPresentation = false
        tableView.tableHeaderView = searchController.searchBar
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        searchController.dismiss(animated: false, completion: nil)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return moviesResults.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:MovieTableViewCell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! MovieTableViewCell
        
        let obj:MovieObjectMapper = self.moviesResults[indexPath.row]
        cell.labelTitle.text = obj.title
    
        let urlImage: String = String.init(format: "http://image.tmdb.org/t/p/w185/%@",obj.imageName!)
            cell.imageMovie.sd_setImage(with: URL(string: urlImage), placeholderImage: UIImage(named: ""))
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        objSelected = self.moviesResults[indexPath.row]
        
        if objSelected != nil {
                self.performSegue(withIdentifier: "segueDetails", sender: self)
        }
        
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueDetails" {
            var vc:DetailsViewController = DetailsViewController()
            vc = segue.destination as! DetailsViewController
            vc.objReceived = objSelected
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    

}

extension SearchViewController {
    
    func updateSearchResults(for searchController: UISearchController) {
        if searchController.searchBar.text! == "" {
            moviesResults = listMovies
        } else {
        
            moviesResults = listMovies.filter {
                ($0.title?.lowercased().contains(searchController.searchBar.text!.lowercased()))!
            }
        }
        tableView.reloadData()
    }
}

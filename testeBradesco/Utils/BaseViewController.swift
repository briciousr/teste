//
//  BaseViewController.swift
//  testeBradesco
//
//  Created by Fabricio Rodrigues on 11/04/2018.
//  Copyright © 2018 Fabricio Rodrigues. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func alertDados() {
        let alertDados:UIAlertController = UIAlertController.init(title: "Atenção", message: "Não foi possível buscar a lista de filmes!", preferredStyle: .alert)
        
        let actionOK: UIAlertAction = UIAlertAction.init(title: "OK", style: .default, handler: nil)
        
        alertDados.addAction(actionOK)
        self.present(alertDados, animated: true, completion: nil)
        
    }
    
}

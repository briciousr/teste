//
//  testeBradescoTests.swift
//  testeBradescoTests
//
//  Created by Fabricio Rodrigues on 11/04/2018.
//  Copyright © 2018 Fabricio Rodrigues. All rights reserved.
//

import XCTest
@testable import testeBradesco

class testeBradescoTests: XCTestCase {
    
    var moviesVC = MoviesViewController()
    var detailsVC = DetailsViewController()
    var searchVC = SearchViewController()
    var req = MoviesRequest()
    
    override func setUp() {
        super.setUp()
        
        let storyboard = UIStoryboard(name: "Main",bundle: nil)
        
        let viewController = storyboard.instantiateViewController(withIdentifier: "MoviesViewController")
        moviesVC = viewController as! MoviesViewController
        moviesVC.loadViewIfNeeded()
        
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testCollection() {
        XCTAssertNotNil(moviesVC.collectionView)
    }
    
    func testRequest() {
        
        MoviesRequest.getListMovies(page: 1) { (movies, error) in
            
            if let newListMovies = movies {
                
                XCTAssertNotNil(newListMovies)
            }
        }
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
